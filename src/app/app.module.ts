import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RocketListComponent } from './rocket-list/rocket-list.component';
import { RocketComponent } from './rocket-list/rocket/rocket.component';

@NgModule({
  declarations: [
    AppComponent,
    RocketListComponent,
    RocketComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
