import { Component, OnInit } from '@angular/core';
import { SpaceXService } from '../shared/space-x.service';
import { Weather } from '../shared/space-x.model';

@Component({
  selector: 'app-rocket-list',
  templateUrl: './rocket-list.component.html',
  styleUrls: ['./rocket-list.component.scss']
})
export class RocketListComponent implements OnInit {
  public parseData: any;
  public isLoader: boolean = false;
  public years: any = [];
  public filter: any = {};
  constructor(private rocketList: SpaceXService) { }

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData() {
    this.isLoader = true;
    this.rocketList.getData().subscribe((data: any) => {
      this.parseData = data;
      this.isLoader = false;
      this.years = data.filter((value:any, index:any, self:any) => self.map((x:any) => x. launch_year).indexOf(value. launch_year) == index);
    })
  }
  launchYear(year: string): void {
    this.isLoader = true;
    this.filter.launch_year = year;
    this.rocketList.getFilterBy(this.filter).subscribe((data: Weather) => {
      this.parseData = data;
      this.isLoader = false;
    })
  }
  launch(launch: boolean): void {
    this.isLoader = true;
    this.filter.launch_success = launch;
    this.rocketList.getFilterBy(this.filter).subscribe((data: Weather) => {
      this.parseData = data;
      this.isLoader = false;
    })
  }
  landing(landing: boolean): void {
    this.isLoader = true;
    this.filter.land_success = landing;
    this.rocketList.getFilterBy(this.filter).subscribe((data: Weather) => {
      this.parseData = data;
      this.isLoader = false;
    })
  }

}
