import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Weather } from './space-x.model';
//import {URLSearchParams} from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class SpaceXService {

  constructor(private http: HttpClient) { }

  public getData():Observable<Weather> {
    const configURL = "https://api.spacexdata.com/v3/launches?limit=100";
    return this.http.get<Weather>(configURL);
  }
  public getFilterBy(filter:any):Observable<Weather> {

    // let params = new URLSearchParams();
    // for (let key in filter) {
    //   if(filter[key]) {
    //   params.set(key, filter[key]);
    //   }
    // }
    // console.log("https://api.spaceXdata.com/v3/launches?limit=100?" + params.toString());

    const launch = Object.keys(filter).includes("launch_success")? "&launch_success=" + filter.launch_success : "";
    const year = Object.keys(filter).includes("launch_year")? "&launch_year=" + filter.launch_year : "";
    const landing = Object.keys(filter).includes("land_success")? "&land_success=" + filter.land_success : "";

    const configURL = "https://api.spaceXdata.com/v3/launches?limit=100" + launch + landing + year;
    return this.http.get<Weather>(configURL);
  }
}
